package graduationProject.servicesTests;

import graduationProject.models.Product;
import graduationProject.services.ProductServiceImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProductServiceTest {

    private final ProductServiceImpl productService = new ProductServiceImpl();

    @Test
    void showProductShouldReturnException() {
        try {
            Product productTest = productService.showProduct(-1);
            fail("При отрицательном id должно быть выброшено исключение!");
        }
        catch (RuntimeException e) {
            System.out.println("Исключение выброшено!");
            assertTrue(true);
        }

    }
}
