package graduationProject.services;

import graduationProject.models.Order;
import graduationProject.models.Person;
import graduationProject.repositories.OrderRepository;
import graduationProject.security.PersonDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderProductServiceImpl orderProductService;
    private final ProductServiceImpl productService;
    private final BasketServiceImpl basketService;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, OrderProductServiceImpl orderProductService,
                            ProductServiceImpl productService, BasketServiceImpl basketService) {
        this.orderRepository = orderRepository;
        this.orderProductService = orderProductService;
        this.productService = productService;
        this.basketService = basketService;
    }

    public List<Order> showListOrders(Person person) {
        return orderRepository.findAllByOrderOwner(person);
    }

    @Transactional
    public void createOrder() {
        Order order = new Order();
        order.setOrderOwner(PersonDetails.getPersonAuth());
        order.setTimeCreatedOrder(new Date());
        orderRepository.save(order);
        orderProductService.createOrderProduct(order);
    }
}
