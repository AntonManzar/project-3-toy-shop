package graduationProject.services;

import graduationProject.models.Product;

import java.util.List;

public interface ProductService {
    List<Product> showListProduct(String sort);

    Product showProduct(int productId);

    void createNewProduct(Product product);

    void editProduct(int productId, Product product);

    void deleteProduct(int productId);
}
