package graduationProject.services;

import graduationProject.models.Person;
import java.util.List;

public interface PersonService {
    List<Person> showListPerson();

    Person showPersonById(int personId);

    void register(Person person);

    void editPerson(int personId, Person person);

    void deletePerson(int personId);

    void createNewPerson(Person person);

    void addRolePerson(int personId);
}
