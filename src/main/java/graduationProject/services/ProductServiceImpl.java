package graduationProject.services;

import graduationProject.models.Product;
import graduationProject.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProductServiceImpl implements ProductService{

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductServiceImpl() {
        productRepository = null;
    }

    public List<Product> showListProduct(String sort) {
        return productRepository.findAll(Sort.by(sort));
    }

    public Product showProduct(int productId) {
        if(productId <= 0) throw new RuntimeException("productId less than zero");
        return productRepository.findById(productId).orElseThrow(RuntimeException::new);
    }

    @Transactional
    public void createNewProduct(Product product) {
        productRepository.save(product);
    }

    @Transactional
    public void editProduct(int productId, Product product) {
        Product updateProduct = productRepository.findById(productId).orElse(null);
        updateProduct.setProductAmount(product.getProductAmount());
        updateProduct.setProductName(product.getProductName());
        updateProduct.setProductPrice(product.getProductPrice());
        productRepository.save(product);
    }

    @Transactional
    public void deleteProduct(int productId) {
        productRepository.deleteById(productId);
    }
}
