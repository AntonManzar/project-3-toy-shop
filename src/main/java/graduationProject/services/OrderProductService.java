package graduationProject.services;

import graduationProject.models.Order;
import graduationProject.models.OrderProduct;

import java.util.List;

public interface OrderProductService {
    List<OrderProduct> showListOrderProduct();

    void createOrderProduct(Order order);
}
