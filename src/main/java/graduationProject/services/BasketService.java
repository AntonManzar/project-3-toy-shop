package graduationProject.services;

import graduationProject.models.Basket;
import graduationProject.models.Person;
import graduationProject.security.PersonDetails;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BasketService {

    List<Basket> showBasketWithProducts(Person person);

    void addBasket(Integer productId, Basket basket);

    void deleteById(Integer id);

    void emptyTheBasket();
}
