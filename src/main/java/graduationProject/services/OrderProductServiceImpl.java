package graduationProject.services;

import graduationProject.models.Order;
import graduationProject.models.OrderProduct;
import graduationProject.models.Product;
import graduationProject.repositories.OrderProductRepository;
import graduationProject.security.PersonDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class OrderProductServiceImpl implements OrderProductService {

    private final OrderProductRepository orderProductRepository;
    private final BasketServiceImpl basketService;
    private final ProductServiceImpl productService;

    public OrderProductServiceImpl(OrderProductRepository orderProductRepository, BasketServiceImpl basketService, ProductServiceImpl productService) {
        this.orderProductRepository = orderProductRepository;
        this.basketService = basketService;
        this.productService = productService;
    }

    public List<OrderProduct> showListOrderProduct() {
        return orderProductRepository.findAll();
    }

    @Transactional
    public void createOrderProduct(Order order) {
        basketService.showBasketWithProducts(PersonDetails.getPersonAuth()).forEach(element -> {
            OrderProduct orderProduct = new OrderProduct();
            orderProduct.setProduct(element.getProduct());
            orderProduct.setOrderAmount(element.getBasketAmount());

            Product product = productService.showProduct(orderProduct.getProduct().getProductId());
            product.setProductAmount(product.getProductAmount() - element.getBasketAmount());
            productService.editProduct(product.getProductId(), product);

            orderProduct.setOrder(order);
            orderProductRepository.save(orderProduct);
        });
        basketService.emptyTheBasket();
    }
}
