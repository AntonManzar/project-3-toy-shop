package graduationProject.services;

import graduationProject.models.Basket;
import graduationProject.models.Person;
import graduationProject.repositories.BasketRepository;
import graduationProject.repositories.ProductRepository;
import graduationProject.security.PersonDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class BasketServiceImpl implements BasketService {

    private final BasketRepository basketRepository;
    private final ProductRepository productRepository;

    @Autowired
    public BasketServiceImpl(BasketRepository basketRepository, ProductRepository productRepository) {
        this.basketRepository = basketRepository;
        this.productRepository = productRepository;
    }

    public List<Basket> showBasketWithProducts(Person person) {
        return basketRepository.findByBasketOwner(person);
    }

    @Transactional
    public void addBasket(Integer productId, Basket basket) {
        basketRepository.findAll().forEach(element -> {
            if ((element.getBasketOwner().getPersonId() == PersonDetails.getPersonAuth().getPersonId())
                    && (productId == element.getProduct().getProductId())) {
                Basket basketSwap = basketRepository.getById(element.getBasketId());
                basket.setBasketAmount(basketSwap.getBasketAmount()+basket.getBasketAmount());
                basket.setBasketId(basketSwap.getBasketId());
            }
        });

        basket.setProduct(productRepository.getById(productId));
        basket.setBasketOwner(PersonDetails.getPersonAuth());
        basketRepository.save(basket);
    }

    @Transactional
    public void deleteById(Integer id) {
        basketRepository.deleteById(id);
    }

    @Transactional
    public void emptyTheBasket() {
        basketRepository.deleteAllByBasketOwner(PersonDetails.getPersonAuth());
    }
}
