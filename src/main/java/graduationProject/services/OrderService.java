package graduationProject.services;

import graduationProject.models.Order;
import graduationProject.models.Person;
import java.util.List;

public interface OrderService {
    List<Order> showListOrders(Person person);

    void createOrder();
}
