package graduationProject.controllers;

import graduationProject.models.Basket;
import graduationProject.models.Product;
import graduationProject.security.PersonDetails;
import graduationProject.services.BasketServiceImpl;
import graduationProject.services.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/product")
public class ProductController {

    private final ProductServiceImpl productService;
    private final BasketServiceImpl basketService;
    public static boolean ifError;

    @Autowired
    public ProductController(ProductServiceImpl productService, BasketServiceImpl basketService) {
        this.productService = productService;
        this.basketService = basketService;
    }

    @GetMapping()
    public String showListProduct(Model model, @ModelAttribute("addBasket") Basket basket, @RequestParam(value = "sort", required = false) String sort) {
        if (sort==null) sort = "productName";
        model.addAttribute("listProduct", productService.showListProduct(sort));
        model.addAttribute("basket", basketService.showBasketWithProducts(PersonDetails.getPersonAuth()));
        if (ifError) {
            model.addAttribute("error", ifError);
            ifError = false;
        }
        return "product/showListProduct";
    }

    @PostMapping()
    public String addProductInBasket(@ModelAttribute("addBasket") Basket basket, @RequestParam("productId") Integer id) {
        basketService.addBasket(id, basket);
        return "redirect:product";
    }

    @DeleteMapping()
    public String deleteProductWithBasket(@RequestParam("basketId") Integer id) {
        basketService.deleteById(id);
        return "redirect:product";
    }

    @GetMapping("/{productId}")
    public String showProduct(@PathVariable("productId") int productId, Model model) {
        model.addAttribute("product", productService.showProduct(productId));
        model.addAttribute("roleAdmin", PersonDetails.getPersonAuth().getPersonRole().equals("ROLE_ADMIN"));
        return "product/showProduct";
    }

    @GetMapping("/createNewProduct")
    public String createNewProduct(@ModelAttribute("product") Product product) {

        return "product/createNewProduct";
    }

    @PostMapping("/createNewProduct")
    public String createNewProductPost(@ModelAttribute("product") Product product) {

        productService.createNewProduct(product);
        return "redirect:/product";
    }

    @GetMapping("/{productId}/editProduct")
    public String editProductGet(@PathVariable("productId") int productId, Model model) {
        model.addAttribute("product", productService.showProduct(productId));

        return "product/editProduct";
    }

    @PatchMapping("/{productId}")
    public String editProductPatch(@ModelAttribute("product") @Valid Product product, @PathVariable("productId") int productId) {

        productService.editProduct(productId, product);

        return "redirect:/product";
    }

    @DeleteMapping("/{productId}")
    public String deleteProduct(@PathVariable("productId") int productId) {
        productService.deleteProduct(productId);
        return "redirect:/product";
    }

}
