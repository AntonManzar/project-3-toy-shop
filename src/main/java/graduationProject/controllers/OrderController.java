package graduationProject.controllers;

import graduationProject.security.PersonDetails;
import graduationProject.services.BasketServiceImpl;
import graduationProject.services.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/order")
public class OrderController {

    private final OrderServiceImpl orderService;
    private final BasketServiceImpl basketService;

    @Autowired
    public OrderController(OrderServiceImpl orderService, BasketServiceImpl basketService) {
        this.orderService = orderService;
        this.basketService = basketService;
    }

    @GetMapping
    public String showListOrders(Model model) {
        model.addAttribute("listOrders", orderService.showListOrders(PersonDetails.getPersonAuth()));
        return "order/showListOrders";
    }

    @PostMapping
    public String createOrder() {
        basketService.showBasketWithProducts(PersonDetails.getPersonAuth()).forEach(element -> {
            if (element.getBasketAmount() > element.getProduct().getProductAmount()) {
                ProductController.ifError = true;
            }
        });
        if (ProductController.ifError) return "redirect:product";
        orderService.createOrder();
        return "redirect:order";
    }

}
