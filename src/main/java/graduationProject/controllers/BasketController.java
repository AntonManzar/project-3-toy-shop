package graduationProject.controllers;

import graduationProject.services.BasketServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/basket")
public class BasketController {

    public final BasketServiceImpl basketService;

    @Autowired
    public BasketController(BasketServiceImpl basketService) {
        this.basketService = basketService;
    }

//    @GetMapping
//    public String showAddBasket() {
//        return "product/showListProduct";
//    }
//
//    @PostMapping
//    public String addBasket(@ModelAttribute("addBasket") Basket basket) {
//        return "product/showListProduct";
//    }
}
